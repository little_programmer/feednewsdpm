﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Net.Http;


namespace APIapp
{
	public partial class DetailPage : ContentPage
	{
		public DPM_newList item = new DPM_newList();
			
		public DetailPage(string  newsID)
		{
			InitializeComponent();

			Task t = GetDetailByID(newsID);
			//lblLoad.IsRunning = false;
			//lblTitle.Text = dpm.Listpdm.TOPIC;
			//lblDetail.Text = dpm.Listpdm.DETAIL;
		}
		public async  Task GetDetailByID(string id)
		{
			if (id == "") return;
			//lblLoad.IsVisible = true;
			//lblLoad.IsRunning = true;
			//int iDex = Convert.ToInt32(id);

			var httpClient = new HttpClient();
			var response = await httpClient.GetAsync("http://dpm.depth1st.com/portal/mobile?cmd=loadNews&newsId=" + id);

			var body = await response.Content.ReadAsStringAsync();

			var json = JObject.Parse(body);

			var row = json["news"];

			DPM_loadNews Listpdm = JsonConvert.DeserializeObject<DPM_loadNews>(row.ToString());

			lblTitle.Text = Listpdm.topic;
			lblDetail.Text = Listpdm.detail;

			row = json["newsImageList"];
			List<DPM_NewsImgList> lstImg = JsonConvert.DeserializeObject<List<DPM_NewsImgList>>(row.ToString());
			//Img.Source = lstImg[0].attachFileName;
			for (int i = 0; i < lstImg.Count; i++)
			{
				lstImg[i].attachFileName = "http://dpm.depth1st.com/portal/download/" + lstImg[i].attachFileName;
			}
			listImg.ItemsSource = lstImg;

			//DPM_loadNews Listpdm = new DPM_loadNews();
			//listDisplayData.ItemsSource = lstDpmNewsList;
			//int i = 0;
			//while (lstDpmNewsList[i].ID!=id)
			//{
			//	i++;
			//}
			//Listpdm = lstDpmNewsList[i];
			//for (int i = 0; i < lstDpmNewsList.Count; i++)
			//{
			//	if (lstDpmNewsList[i].ID == id)
			//	{
			//		Listpdm = lstDpmNewsList[i];
			//	}
			//}
			//foreach (DPM_loadNews iTem in lstDpmNewsList)
			//{
			//	//if (id==iTem.ID)
			//	//{
			//		//Listpdm = iTem;
			//		lblTitle.Text = iTem.topic;
			//		lblDetail.Text = iTem.detail;
			//	//}
			//}
			//var filteredOrders = orders.Order.Where(o => allowedStatus.Contains(o.StatusCode));
			//Listpdm = lstDpmNewsList.Where((b) => id.Contains(b.ID));
			//var lstDpm = from d in lstDpmNewsList where d.ID == id select d;
			//Listpdm = (DPM_newList)lstDpm;
			Task.Delay(5000);
			//lblLoad.IsRunning = false;
			overload.IsVisible = false;

		}
	}
}
