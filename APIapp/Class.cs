﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;

namespace APIapp
{
	
	public class DPM_news
	{
		public string ACD_ID { get; set; }
		public string ACD_ROAD{ get; set; }
		public string GPS_LAT{ get; set; }
		public string GPS_LNG{ get; set; }
		public string ACD_DATE{ get; set; }
		public string ACD_DATECASE{ get; set; }
		public string ACD_ADMIT{ get; set; }
		public string ACD_DEAD{ get; set; }
		public string GIS_E{ get; set; }
		public string GIS_N{ get; set; }
	}
	public class DPM_paramNew
	{
		public string queryCode { get; set; }
		public string STARTSATE { get; set; }
		public string ENDDATE { get; set; }
		 
	}
	public class DPM_newList
	{
		public string TOPIC { get; set; }
		public string DETAIL { get; set; }
		public string STATUS { get; set; }
		public string reportedBy { get; set; }
		public string lastestImage { get; set; } = "http://www.swpark.or.th/components/com_jumi/files/tenant/Company_Logo/21062012124225logo_df_%E0%B9%82%E0%B8%9B%E0%B8%A3%E0%B9%88%E0%B8%87.png";
		public string ID { get; set; }
		public string disasterTypeLabel { get; set; }
		public string DATEeng { get; set; }
		public string disasterType { get; set; }
		public string provinceLabel { get; set; }
		public string longitude { get; set; }
		public string latitude { get; set; }
		public string groupLabel { get; set; }
		public string lastUpdDateEng { get; set; }


		//public async  Task GetDetailByID(string id)
		//{
			 
		//	var httpClient = new HttpClient();
		//	var response = await httpClient.GetAsync("http://dpm.depth1st.com/portal/mobile?cmd=listNews&pageSize=20&position=0&stats=1&status=2");
		//	var body = await response.Content.ReadAsStringAsync();

		//	var json = JObject.Parse(body);

		//	var row = json["newsList"];

		//	List<DPM_newList> lstDpmNewsList = JsonConvert.DeserializeObject<List<DPM_newList>>(row.ToString());
		//	//Listpdm = lstDpmNewsList[0];
		//	//listDisplayData.ItemsSource = lstDpmNewsList;
		//}
		//public List<DPM_newList> Get2Display(List<DPM_newList> item)
		//{
		//	return item;
		//}
	}
	public class DPM_paramNweList
	{
		//params: cmd=listNews
		//pageSize = 20
		//position=0
		//stats=1
		//status=2
		public string cmd { get; set; }
		public string pageSize { get; set; }
		public string position { get; set; }
		public string stats { get; set; }
		public string status { get; set; }
			
	}
	public class DPM_loadNews
	{
		//id,disasterType,disasterTypeLabel,topic,detail,latitude,longitude,dateEng,provinceLabel,lastUpdDateEng,reportedBy,status,,,,
		public string id { get; set; }
		public string disasterType { get; set; }
		public string disasterTypeLabel { get; set; }
		public string topic { get; set; }
		public string detail { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string dateEng { get; set; }
		public string provinceLabel { get; set; }
		public string lastUpdDateEng { get; set; }
		public string reportedBy { get; set; }
		public string status { get; set; }
		//public List<DPM_NewsImgList> lstDpmNewsImage { get; set; }

	}
	public class DPM_NewsImgList
	{
		//fileId,attachFileIdx,attachFileName
		public string fileId { get; set; }
		public string attachFileIdx { get; set; }
		public string attachFileName { get; set; }
	}


}
